##  Application

Start the app by following these steps.

After cloning repo

 Run the below command

``` bash
# Serve at localhost:8080.
`npm install`


`npm start`

## Built using:
 - Javascript
 - Node.js
 - ReactJS 
 - Webpack
 
## For timing no styling was applied
## If I had more time I would have applied CSS and flexbox for more style

## Purpose:
 Code Challenge/Barrel Developer test

### Looking Ahead:

Learn more aobut BEM AND CSS Grid

### QUICK NOTES

• Ran out of time, not all points were accomplished. 

•  It was interesting learning BEM and not using Flexbox wish I had more time to implement them better. Project view will be a little weird.

• Loved Barrel's javascript stanards, definitely compares to my usual to go[AirBnb](https://github.com/airbnb/javascript)


