  
const React = require("react");
const ReactDOM =require("react-dom");


class TopPage extends React.Component {
render() {
    return (
  <div >
    <nav>
           <p className="sponsor">Sponsored By</p>
                <img  className="main-logo" src="../_assets/Logo/www.knobcreek.com-1311011787501770.svg" />
            <div className="nav--info">
                <button  type="button">LOGO</button>
               <div className="nav--social">
                    <img className='social-icons' src='https://globies.bostonglobe.com/wp-content/themes/globies2/images/icon-facebook-tan.svg' />
                     <img className='social-icons' src='https://globies.bostonglobe.com/wp-content/themes/globies2/images/icon-twitter-tan.svg' />
                </div>
      </div>
    </nav>
    <header>

     <div className="second--info">

        <div className="second--main">
          <div className="second-main-info">
            <h1 className="mainSnippet">Maple Never Tasted So Good</h1>
            <p>
              We blend this bourbon with natural smoked maple flavors for a unique,
              smokey sweetness. Full bodied, inviting maple notes that lift to smoke
              and are complemented with rich vanilla caramel. Smoked hickory and maple
              wood, with hints of earthy grains.
                <img className="main-image"  src="../_assets/photography/www.knobcreek.com-1293609732325191.jpg"/>
            </p>
          </div>
             <div className="bourbon--img">
              <img  src="../_assets/photography/www.knobcreek.com-1310894113736742.png" />
             </div>
           </div>
         <div className="second--sub-info">
          <div className="second--recent">
            <div className="article--img">
              <img src='../_assets/photography/pour-mug.jpg' width="50%" />
            </div>
            <div className="second--articles">
              <div className="second-article">
                <img className="icon" src="../_assets/iconography/Article.svg" />
                <p className="date">NOVEMBER 20</p>
                <h3>Too Much Flavor For Four Walls To Hold</h3>
                <p>
                  There are only so many hours in a day, so we make every glass count.
                  We craft full-flavorred whiskey for those who find a way to get
                  the most out of every minute of the every day...
                </p>
                <a href="#">Read More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      </header>

</div>);
  }
}
export default TopPage