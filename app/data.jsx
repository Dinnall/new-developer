
const articles =[
{
        src: "../_assets/photography/www.knobcreek.com-1550031134660845.jpg",
        icon: "../_assets/iconography/Article.svg ",
        date: " NOVEMBER 10",
        tagline: " This Creek Runs Full, Making Every Glass Count",
        modalInfo: " ",
},{
        src: "../_assets/photography/www.knobcreek.com-1564585437648699.jpg",
        icon: "../_assets/iconography/Video.svg",
        date: "NOVEMBER 01",
        tagline: "Celebrating Knob Creek's 25th Anniversary",
        modalInfo: "",
},
{
        src: "../_assets/photography/www.knobcreek.com-1550246487292757.jpg",
        icon: "../_assets/iconography/Glass.svg ",
        date: "OCTOBER 30",
        tagline: "This Single Barrel Experience",
        modalInfo: '',
},
{
        src: "../_assets/photography/www.knobcreek.com-1550164189475220.jpg",
        icon: "../_assets/iconography/Video.svg",
        date: "OCTOBER 20",
        tagline: "Make No Small Plans.Drink No Small Bourbon",
        modalInfo:"" ,
},{
        src: "../_assets/photography/www.knobcreek.com-1550293000153014.jpg ",
        icon: "../_assets/iconography/Video.svg",
        date: "OCTOBER 10",
        tagline: "There's No Faking Flavor: It Has To Be Earned",
        modalInfo: "",
},
{
        src: "../_assets/photography/www.knobcreek.com-1550664938574687.jpg ",
        icon: "../_assets/iconography/Article.svg",
        date: "OCTOBER 05",
        tagline: "Learn about Pre-Prohibition Style Whiskey",
        modalInfo:"" ,
},
{
        src: "../_assets/photography/www.knobcreek.com-1550653084056276.jpg ",
        icon: "../_assets/iconography/Gallery.svg",
        date: "SEPTEMBER 30",
        tagline: "About Our Master DIstiller Booker Noe",
        modalInfo: "",
},{
        src: "../_assets/photography/www.knobcreek.com-1550660375610157.jpg ",
        icon: "../_assets/iconography/Glass.svg",
        date: "SEPTEMBER 18",
        tagline: "About Our Master DIstiller Booker No",
        modalInfo: "",
},
{
        src: "../_assets/photography/www.knobcreek.com-1550319714280280.jpg",
        icon: "../_assets/iconography/Gallery.svg",
        date: "SEPTEMBER 10",
        tagline: "Unmistakable Richness and Signature Sweetness",
        modalInfo: "",
}]



export default articles;


