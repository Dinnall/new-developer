const React = require("react");
const ReactDOM =require("react-dom");

import articles from "./data.jsx";

class BlogInfo extends React.Component {
blogs(articles){
    return (
      <div>
           <main>
                 <div className="main__container">
                         <div className="articles">
                         <img className="article__img" src={articles.src} />
                         <div  className="icon--img">
                             <img className="icon" src={articles.icon}/>
                         </div>
                         <p className="date">{articles.date}</p>
                         <p className=" "> {articles.tagline}</p>
                      </div>
                 </div>
             </main>
         </div>
    );
  }
 render(){
    return (
       <div>
         <ul className="flex-container" >
            <h2>Recent Articles</h2>
           {articles.map(articleObj => this.blogs(articleObj))}
         </ul>
      </div>
    );
  }
}
export default BlogInfo

  



