const React = require("react");
const ReactDOM =require("react-dom");

const Footer = () => (
  <footer>
       <div className='footer-info'>
         <div className='share-section'>
          <p>SHARE ON</p>
              <img className='footer-facebook' src='https://globies.bostonglobe.com/wp-content/themes/globies2/images/icon-facebook-tan.svg' />
              <img className='footer-twitter' src='https://globies.bostonglobe.com/wp-content/themes/globies2/images/icon-twitter-tan.svg' />
            <div>
              <img className="footer--logo" src="../_assets/Logo/www.knobcreek.com-1311011787501770.svg" />
            </div>
          </div>
      </div>
  </footer>
);

export default Footer;


