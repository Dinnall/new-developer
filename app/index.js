const React = require("react");
const ReactDOM =require("react-dom");
import BlogGrid from "./blogGrid.js";
import articles from "./data.jsx";
import TopPage from "./topPage.js"
import Footer from "./footer.js"

require('./index.css');

class App extends React.Component {
render() {
    return (
      <div>
         <TopPage />
         <BlogGrid />
         <Footer />
        {this.props.children}
      </div>
    );
  }
}
ReactDOM.render(
    <App />,
    document.getElementById("app")
  );





